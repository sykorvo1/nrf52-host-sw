#!/usr/bin/python3

from intelhex import IntelHex
import sys

BIN_BLOCK_MAX = 0x1000
BIN_LIMIT_ADDR = 0x40000

SDK_PATH = '/home/ladislav/WORK/PROJECT_BLE/SDK/nRF5_SDK_17.0.2'

DEFAULT_APP_PATH = '_build/nrf52833_xxaa'
DEFAULT_SD_PATH = '{}/components/softdevice/s113/hex/s113_nrf52_7.2.0_softdevice'.format(SDK_PATH)
# DEFAULT_OUT_PATH = DEFAULT_APP_PATH

app_hex_path = DEFAULT_APP_PATH if len(sys.argv) <= 1 else sys.argv[1]
sd_hex_path = DEFAULT_SD_PATH if len(sys.argv) <= 2 else sys.argv[2]
out_path_base = app_hex_path


def hex_to_bin(filename, hexfile, start_addr):
    with open(filename, "wb") as f:
        max_addr = hexfile.maxaddr() + 1
        if max_addr > BIN_LIMIT_ADDR: max_addr = BIN_LIMIT_ADDR
        print(filename, ": minaddr 0x%05x maxaddr 0x%05x\n" % (start_addr, max_addr))

        addr = start_addr
        while addr < max_addr:
            blk_len = BIN_BLOCK_MAX

            if addr + blk_len > max_addr: blk_len = max_addr - addr

            buf = hexfile.tobinarray(start=addr, size=blk_len)
            f.write(buf)
            addr += blk_len


if __name__ == '__main__':
    print(app_hex_path, sd_hex_path)
    app_hex_file = IntelHex(app_hex_path + '.hex')
    sd_hex_file = IntelHex(sd_hex_path + '.hex')
    # create binary soft device
    hex_to_bin(out_path_base + '_sd.bin', sd_hex_file, 0x0)

    # merge (soft device + app)
    sd_hex_file.merge(app_hex_file)

    # save (soft device + app)
    with open(out_path_base + '_sd_app.hex', "w") as f:
        sd_hex_file.tofile(f, format='hex')
    sd_hex_file.padding = 0xff

    # create binary app
    hex_to_bin(out_path_base + '_app.bin', app_hex_file, app_hex_file.minaddr())
    # create binary (soft device + app)
    hex_to_bin(out_path_base + '_sd_app.bin', sd_hex_file, 0x0)
